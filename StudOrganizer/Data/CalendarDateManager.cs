﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StudOrganizer.Data
{
    public class CalendarDateManager
    {
        const string Url = "https://nine.wi.hm.edu/api/v2/calendar/day";

        public async Task<List<CalendarDate>> GetCalendarDates(string userId)
        {
            PostCalendarDates postCalendarDatesToday = new PostCalendarDates()
            {
                userid = userId,
                date = "2019-01-15T09:00:07.231125+01:00"
            };

            Console.WriteLine(postCalendarDatesToday.userid.ToString() + "\n" + postCalendarDatesToday.date);

            string content = JsonConvert.SerializeObject(postCalendarDatesToday);
            StringContent stringContent = new StringContent(
                content,
                Encoding.UTF8,
                "application/json");
            using (HttpClient client = new HttpClient())
            {
                List<CalendarDate> calendarDatesToday = new List<CalendarDate>();

                try
                {
                    var response = await client.PostAsync(Url, stringContent);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        calendarDatesToday = JsonConvert.DeserializeObject<List<CalendarDate>>(responseBody);
                        Console.WriteLine(calendarDatesToday[0].shortName);
                        return calendarDatesToday;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n Exception caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                    return calendarDatesToday;
                }
                return calendarDatesToday;
            }
        }
    }
}
