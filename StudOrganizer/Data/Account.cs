﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudOrganizer.Data
{
    public class User
    {
        public string id { get; set; }
        public string firstName { get; set;}
        public string lastName { get; set; }
    }

    public class Organiser
    {
        public string color { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string id { get; set; }
        public List<object> actions { get; set; }
    }

    public class Curriculum
    {
        public Organiser organiser { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string id { get; set; }
        public List<object> actions { get; set; }
    }

    public class Account
    {
        public User user { get; set; }
        public Curriculum curriculum { get; set; }
    }

    public class Login
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
