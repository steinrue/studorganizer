﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudOrganizer.Data
{
    public class CalendarDate
    {
        public Subscription subscription { get; set; }
        public Date date { get; set; }
        public int type { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string id { get; set; }
        public List<object> actions { get; set; }
    }

    public class Subscription
    {
        public string courseId { get; set; }
        public bool isValid { get; set; }
        public bool onWaitingList { get; set; }
        public string message { get; set; }
    }

    public class Room
    {
        public string number { get; set; }
        public string building { get; set; }
        public string campus { get; set; }
        public string description { get; set; }
        public string id { get; set; }
        public List<object> actions { get; set; }
    }

    public class Lecturer
    {
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string faculty { get; set; }
        public string id { get; set; }
        public List<object> actions { get; set; }
    }

    public class Date
    {
        public string begin { get; set; }
        public string end { get; set; }
        public DateTime from { get; set; }
        public DateTime until { get; set; }
        public string title { get; set; }
        public bool isCanceled { get; set; }
        public List<Room> rooms { get; set; }
        public List<Lecturer> lecturer { get; set; }
        public string id { get; set; }
        public List<object> actions { get; set; }
    }

    public class PostCalendarDates
    {
        public string userid { get; set; }
        public string date { get; set; }
    }
}
