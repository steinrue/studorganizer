﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StudOrganizer.Data
{
    public static class UserManager
    {
        const string Url = "https://nine.wi.hm.edu/api/v2/account/login";
        public static string userID { get; set; }

        public static async Task<Boolean> Login(string username, string password)
        {
            Login loginCredentials = new Login()
            {
                username = username,
                password = password
            };

            string content = JsonConvert.SerializeObject(loginCredentials);
            StringContent stringContent = new StringContent(
                content,
                Encoding.UTF8,
                "application/json");
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var response = await client.PostAsync(Url, stringContent);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        Account userAccount = JsonConvert.DeserializeObject<Account>(responseBody);
                        userID = userAccount.user.id;
                        Console.WriteLine(userAccount.user.lastName);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("\n Exception caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                    return false;
                }
            }
        }
    }
}
