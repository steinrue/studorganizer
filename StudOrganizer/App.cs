﻿using Plugin.Connectivity;
using System;
using Xamarin.Forms;

namespace StudOrganizer
{
	public class App : Application
	{
		public static bool IsUserLoggedIn { get; set; }

		public App ()
		{
            // Cross Connectivity from XAM150, also iOS app transport security ATS
            if(!CrossConnectivity.Current.IsConnected)
            {
                MainPage = new NavigationPage(new NoNetworkOnLoginPage());
            }
			else if (!IsUserLoggedIn) {
				MainPage = new NavigationPage(new LoginPage());
			} else {
				MainPage = new NavigationPage (new MainPage());
			}
		}

		protected override void OnStart ()
		{
            // Handle when your app starts
            base.OnStart();
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
		}

        private void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            Type currentPage = this.MainPage.GetType();
            if(e.IsConnected && currentPage != typeof(LoginPage) && !IsUserLoggedIn)
            {
                this.MainPage = new NavigationPage(new LoginPage());
            } else if(!e.IsConnected && currentPage != typeof(NoNetworkOnLoginPage))
            {
                this.MainPage = new NavigationPage(new NoNetworkOnLoginPage());
            } else if (e.IsConnected && IsUserLoggedIn && currentPage != typeof(MainPage))
            {
                this.MainPage = new NavigationPage(new MainPage());
            }
        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

