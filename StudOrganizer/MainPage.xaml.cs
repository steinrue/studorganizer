﻿using StudOrganizer.Data;
using StudOrganizer.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace StudOrganizer
{
	public partial class MainPage : ContentPage
	{
        public ObservableCollection<CalendarDatesTodayViewModel> CalendarDatesToday { get; set; }
        public  MainPage()
        {
            InitializeComponent();
            this.IsBusy = true;
            CalendarDatesToday = new ObservableCollection<CalendarDatesTodayViewModel>();
            CalendarDatesTodayListView.ItemsSource = CalendarDatesToday;
            this.IsBusy = false;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var calendarDates = await GetCalendarDatesToday();
            CalendarDatesToday.Clear();

            foreach (var calendarDate in calendarDates)
            {
                CalendarDatesToday.Add(new CalendarDatesTodayViewModel()
                {
                    lastName = calendarDate.date.lecturer[0].lastName,
                    number = calendarDate.date.rooms[0].number,
                    from = calendarDate.date.from,
                    until = calendarDate.date.until,
                    name = calendarDate.name
                });
            }
        }
        async Task<List<CalendarDate>> GetCalendarDatesToday()
        {
            CalendarDateManager calendarDateManager = new CalendarDateManager();
            return await calendarDateManager.GetCalendarDates(UserManager.userID);
        }

		async void OnLogoutButtonClicked (object sender, EventArgs e)
		{
			App.IsUserLoggedIn = false;
			Navigation.InsertPageBefore (new LoginPage (), this);
			await Navigation.PopAsync ();
		}

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            await DisplayAlert("Item Tapped", "An item was tapped.", "OK");

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}
