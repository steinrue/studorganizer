﻿using StudOrganizer.Data;
using System;
using Xamarin.Forms;

namespace StudOrganizer
{
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		}

		async void OnSignUpButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new SignUpPage ());
		}

		async void OnLoginButtonClicked (object sender, EventArgs e)
		{
            this.IsBusy = true;
			var login = new Login {
				username = usernameEntry.Text,
				password = passwordEntry.Text
			};

            bool loginIsSuccessful = await UserManager.Login(login.username, login.password);

            this.IsBusy = false;

			if (loginIsSuccessful) {
				App.IsUserLoggedIn = true;
				Navigation.InsertPageBefore(new MainPage (), this);
                await Navigation.PopAsync();
			} else {
				messageLabel.Text = "Login failed";
				passwordEntry.Text = string.Empty;
			}
		}
	}
}
