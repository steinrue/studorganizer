﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace StudOrganizer.ViewModels
{
    public class CalendarDatesTodayViewModel
    {
        public string lastName { get; set; }
        public string number { get; set; }
        public DateTime from { get; set; }
        public DateTime until { get; set; }
        public string name { get; set; }
        public string fromUntil { get => from.ToShortTimeString() + " - " + until.ToShortTimeString(); set { } }
    }
}
